Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: CPU-X
Upstream-Contact: X0rg (https://github.com/X0rg/CPU-X)
Source: https://github.com/X0rg/CPU-X

Files: .cirrus.yml
 .github/ISSUE_TEMPLATE.md
 .github/ISSUE_TEMPLATE/bug_report.md
 .github/ISSUE_TEMPLATE/feature_request.md
 .github/ISSUE_TEMPLATE/libcpuid.md
 .github/main.workflow
 .github/workflows/appimage_continuous.yml
 .github/workflows/appimage_release.yml
 .github/workflows/linux.yml
 .github/workflows/update_pot_file.yml
 .gitignore
 CMakeLists.txt
 CONTRIBUTING.md
 ChangeLog.md
 README.md
 cmake/FindFilesystem.cmake
 cmake/GSettings.cmake
 cmake/cmake_uninstall.cmake.in
 cmake/gettext_create_translations.cmake
 data/CMakeLists.txt
 data/completions/CMakeLists.txt
 data/completions/bash
 data/completions/fish
 data/completions/zsh
 data/cpu-x-gtk-3.12-dark.css
 data/cpu-x-gtk-3.12.css
 data/cpu-x-gtk-3.12.ui
 data/cpu-x-gtk-3.20-dark.css
 data/cpu-x-gtk-3.20.css
 data/icons/CMakeLists.txt
 data/icons/CPU-X_128x128.png
 data/icons/CPU-X_16x16.png
 data/icons/CPU-X_192x192.png
 data/icons/CPU-X_22x22.png
 data/icons/CPU-X_24x24.png
 data/icons/CPU-X_256x256.png
 data/icons/CPU-X_32x32.png
 data/icons/CPU-X_36x36.png
 data/icons/CPU-X_384x384.png
 data/icons/CPU-X_48x48.png
 data/icons/CPU-X_512x512.png
 data/icons/CPU-X_64x64.png
 data/icons/CPU-X_72x72.png
 data/icons/CPU-X_96x96.png
 data/icons/CPU-X_original.png
 data/io.github.thetumultuousunicornofdarkness.cpu-x-daemon.policy.in
 data/io.github.thetumultuousunicornofdarkness.cpu-x.desktop.in
 data/io.github.thetumultuousunicornofdarkness.cpu-x.gschema.xml
 data/logos/CMakeLists.txt
 data/logos/AMD.png
 data/logos/Centaur.png
 data/logos/Cyrix.png
 data/logos/Intel.png
 data/logos/NSC.png
 data/logos/NexGen.png
 data/logos/Rise.png
 data/logos/SiS.png
 data/logos/Transmeta.png
 data/logos/UMC.png
 data/logos/Unknown.png
 po/CMakeLists.txt
 scripts/build_appimage.sh
 scripts/build_libcpuid.sh
 scripts/build_ubuntu.sh
 scripts/check_new_strings.sh
 scripts/patch_dmidecode.sh
 scripts/run_libcpuid_tests.sh
 src/CMakeLists.txt
 src/bandwidth/CMakeLists.txt
 src/bandwidth/README.txt
 src/dmidecode/.hash
 src/dmidecode/AUTHORS
 src/dmidecode/CMakeLists.txt
 src/dmidecode/NEWS
 src/dmidecode/README
 src/dmidecode/config.h
 src/dmidecode/types.h
 tests/CMakeLists.txt
 tests/awk/results/amdgpu_pm_info
 tests/awk/results/aticonfig-odgc-load
 tests/awk/results/aticonfig-odgc-mclk
 tests/awk/results/aticonfig-odgc-sclk
 tests/awk/results/aticonfig-odgt
 tests/awk/results/nvidia-settings-GPUCurrentClockFreqs-mclk
 tests/awk/results/nvidia-settings-GPUCurrentClockFreqs-sclk
 tests/awk/results/nvidia-settings-GPUUtilization
 tests/awk/results/pp_dpm_mclk
 tests/awk/results/pp_dpm_sclk
 tests/awk/results/radeon_pm_info-mclk
 tests/awk/results/radeon_pm_info-sclk
 tests/awk/results/radeon_pm_info-vddc
 tests/awk/samples/amdgpu_pm_info
 tests/awk/samples/aticonfig-odgc
 tests/awk/samples/aticonfig-odgt
 tests/awk/samples/nvidia-settings-GPUCurrentClockFreqs
 tests/awk/samples/nvidia-settings-GPUUtilization
 tests/awk/samples/pp_dpm_mclk
 tests/awk/samples/pp_dpm_sclk
 tests/awk/samples/radeon_pm_info
 tests/awk/test_regex.sh
 tests/common.sh
 tests/grep/results/nouveau_pstate_first_core
 tests/grep/results/nouveau_pstate_first_memory
 tests/grep/results/nouveau_pstate_forced_core
 tests/grep/results/nouveau_pstate_forced_memory
 tests/grep/samples/nouveau_pstate_first
 tests/grep/samples/nouveau_pstate_forced
 tests/grep/test_regex.sh
 vagrant/Vagrantfile
 vagrant/bootstrap_common.sh
Copyright: 2014-2024, The Tumultuous Unicorn Of Darkness
License: GPL-3+
Comment:
 Assuming copyright holdership from other files in
 this project.
 .
 Assuming license from other files in this project.

Files: data/io.github.thetumultuousunicornofdarkness.cpu-x.appdata.xml
Copyright: 2014-2024, The Tumultuous Unicorn Of Darkness
License: CC0-1.0

Files: src/bandwidth/libbandwidth.h
 src/core.cpp
 src/core.hpp
 src/daemon.h
 src/daemon_client.cpp
 src/daemon_client.hpp
 src/daemon_server.cpp
 src/daemon_server.hpp
 src/data.hpp
 src/databases.h
 src/dmidecode/libdmidecode.h
 src/gui_gtk.cpp
 src/gui_gtk.hpp
 src/logger.cpp
 src/logger.hpp
 src/main.cpp
 src/options.cpp
 src/options.hpp
 src/tui_ncurses.cpp
 src/tui_ncurses.hpp
 src/util.cpp
Copyright: 2014-2024, The Tumultuous Unicorn Of Darkness
License: GPL-3+

Files: src/dmidecode/dmidecode.h
 src/dmidecode/dmioem.c
 src/dmidecode/dmioem.h
 src/dmidecode/dmiopt.c
 src/dmidecode/dmiopt.h
 src/dmidecode/dmioutput.c
 src/dmidecode/dmioutput.h
 src/dmidecode/util.c
 src/dmidecode/util.h
Copyright: 2002-2018, Jean Delvare <jdelvare@suse.de>
  2003-2017, Jean Delvare <jdelvare@suse.de>
  2005-2008, Jean Delvare <jdelvare@suse.de>
  2005-2020, Jean Delvare <jdelvare@suse.de>
  2007-2008, Jean Delvare <jdelvare@suse.de>
  2007-2020, Jean Delvare <jdelvare@suse.de>
  2020, Jean Delvare <jdelvare@suse.de>
License: GPL-2+

Files: src/bandwidth/BMP.c
 src/bandwidth/BMP.h
 src/bandwidth/font.c
 src/bandwidth/font.h
 src/bandwidth/minifont.c
 src/bandwidth/minifont.h
Copyright: 2009-2014, Zack T Smith.
License: GPL-2

Files: src/bandwidth/BMPGraphing.c
 src/bandwidth/BMPGraphing.h
 src/bandwidth/defs.h
 src/bandwidth/routines-x86-32bit.asm
 src/bandwidth/routines-x86-64bit.asm
Copyright: 2005-2015, Zack T Smith.
  2005-2017, Zack T Smith.
License: GPL-2+

Files: src/bandwidth/README.md
Copyright: 2014-2024, The Tumultuous Unicorn Of Darkness
License: GPL-2

Files: src/dmidecode/README.md
Copyright: 2014-2024, The Tumultuous Unicorn Of Darkness
License: GPL-2+

Files: po/cpu-x.pot
 po/*.po
Copyright: 2014-2024, TheTumultuousUnicornOfDarkness
License: GPL-3+

Files: src/opencl_ext.h
Copyright: 2008-2020, The Khronos Group Inc.
License: Apache-2.0

Files: src/dmidecode/dmidecode.c
Copyright: 2000-2002, Alan Cox <alan@redhat.com>
  2002-2020, Jean Delvare <jdelvare@suse.de>
License: GPL-2+

Files: src/bandwidth/main.c
Copyright: 2005-2017, Zack T Smith
  2005-2018, Zack T Smith
License: GPL-2+

Files: src/util.hpp
Copyright: 2014-2024, The Tumultuous Unicorn Of Darkness
License: GPL-3+

Files: src/data.cpp
Copyright: 2014-2024, The Tumultuous Unicorn Of Darkness
License: GPL-3+

Files: debian/*
Copyright: 2019-2024, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+ or GPL-3+ or CC0-1.0

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2
 as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
      http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache 2.0 License
 can be found in /usr/share/common-licenses/Apache-2.0 file.

License: CC0-1.0
 Creative Commons Legal Code
 .
 CC0 1.0 Universal
 .
    CREATIVE COMMONS CORPORATION IS NOT A LAW FIRM AND DOES NOT PROVIDE
    LEGAL SERVICES. DISTRIBUTION OF THIS DOCUMENT DOES NOT CREATE AN
    ATTORNEY-CLIENT RELATIONSHIP. CREATIVE COMMONS PROVIDES THIS
    INFORMATION ON AN "AS-IS" BASIS. CREATIVE COMMONS MAKES NO WARRANTIES
    REGARDING THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS
    PROVIDED HEREUNDER, AND DISCLAIMS LIABILITY FOR DAMAGES RESULTING FROM
    THE USE OF THIS DOCUMENT OR THE INFORMATION OR WORKS PROVIDED
    HEREUNDER.
 .
 Statement of Purpose
 .
 The laws of most jurisdictions throughout the world automatically confer
 exclusive Copyright and Related Rights (defined below) upon the creator
 and subsequent owner(s) (each and all, an "owner") of an original work of
 authorship and/or a database (each, a "Work").
 .
 Certain owners wish to permanently relinquish those rights to a Work for
 the purpose of contributing to a commons of creative, cultural and
 scientific works ("Commons") that the public can reliably and without fear
 of later claims of infringement build upon, modify, incorporate in other
 works, reuse and redistribute as freely as possible in any form whatsoever
 and for any purposes, including without limitation commercial purposes.
 These owners may contribute to the Commons to promote the ideal of a free
 culture and the further production of creative, cultural and scientific
 works, or to gain reputation or greater distribution for their Work in
 part through the use and efforts of others.
 .
 For these and/or other purposes and motivations, and without any
 expectation of additional consideration or compensation, the person
 associating CC0 with a Work (the "Affirmer"), to the extent that he or she
 is an owner of Copyright and Related Rights in the Work, voluntarily
 elects to apply CC0 to the Work and publicly distribute the Work under its
 terms, with knowledge of his or her Copyright and Related Rights in the
 Work and the meaning and intended legal effect of CC0 on those rights.
 .
 1. Copyright and Related Rights. A Work made available under CC0 may be
 protected by copyright and related or neighboring rights ("Copyright and
 Related Rights"). Copyright and Related Rights include, but are not
 limited to, the following:
 .
   i. the right to reproduce, adapt, distribute, perform, display,
      communicate, and translate a Work;
  ii. moral rights retained by the original author(s) and/or performer(s);
 iii. publicity and privacy rights pertaining to a person's image or
      likeness depicted in a Work;
  iv. rights protecting against unfair competition in regards to a Work,
      subject to the limitations in paragraph 4(a), below;
   v. rights protecting the extraction, dissemination, use and reuse of data
      in a Work;
  vi. database rights (such as those arising under Directive 96/9/EC of the
      European Parliament and of the Council of 11 March 1996 on the legal
      protection of databases, and under any national implementation
      thereof, including any amended or successor version of such
      directive); and
 vii. other similar, equivalent or corresponding rights throughout the
      world based on applicable law or treaty, and any national
      implementations thereof.
 .
 2. Waiver. To the greatest extent permitted by, but not in contravention
 of, applicable law, Affirmer hereby overtly, fully, permanently,
 irrevocably and unconditionally waives, abandons, and surrenders all of
 Affirmer's Copyright and Related Rights and associated claims and causes
 of action, whether now known or unknown (including existing as well as
 future claims and causes of action), in the Work (i) in all territories
 worldwide, (ii) for the maximum duration provided by applicable law or
 treaty (including future time extensions), (iii) in any current or future
 medium and for any number of copies, and (iv) for any purpose whatsoever,
 including without limitation commercial, advertising or promotional
 purposes (the "Waiver"). Affirmer makes the Waiver for the benefit of each
 member of the public at large and to the detriment of Affirmer's heirs and
 successors, fully intending that such Waiver shall not be subject to
 revocation, rescission, cancellation, termination, or any other legal or
 equitable action to disrupt the quiet enjoyment of the Work by the public
 as contemplated by Affirmer's express Statement of Purpose.
 .
 3. Public License Fallback. Should any part of the Waiver for any reason
 be judged legally invalid or ineffective under applicable law, then the
 Waiver shall be preserved to the maximum extent permitted taking into
 account Affirmer's express Statement of Purpose. In addition, to the
 extent the Waiver is so judged Affirmer hereby grants to each affected
 person a royalty-free, non transferable, non sublicensable, non exclusive,
 irrevocable and unconditional license to exercise Affirmer's Copyright and
 Related Rights in the Work (i) in all territories worldwide, (ii) for the
 maximum duration provided by applicable law or treaty (including future
 time extensions), (iii) in any current or future medium and for any number
 of copies, and (iv) for any purpose whatsoever, including without
 limitation commercial, advertising or promotional purposes (the
 "License"). The License shall be deemed effective as of the date CC0 was
 applied by Affirmer to the Work. Should any part of the License for any
 reason be judged legally invalid or ineffective under applicable law, such
 partial invalidity or ineffectiveness shall not invalidate the remainder
 of the License, and in such case Affirmer hereby affirms that he or she
 will not (i) exercise any of his or her remaining Copyright and Related
 Rights in the Work or (ii) assert any associated claims and causes of
 action with respect to the Work, in either case contrary to Affirmer's
 express Statement of Purpose.
 .
 4. Limitations and Disclaimers.
 .
  a. No trademark or patent rights held by Affirmer are waived, abandoned,
     surrendered, licensed or otherwise affected by this document.
  b. Affirmer offers the Work as-is and makes no representations or
     warranties of any kind concerning the Work, express, implied,
     statutory or otherwise, including without limitation warranties of
     title, merchantability, fitness for a particular purpose, non
     infringement, or the absence of latent or other defects, accuracy, or
     the present or absence of errors, whether or not discoverable, all to
     the greatest extent permissible under applicable law.
  c. Affirmer disclaims responsibility for clearing rights of other persons
     that may apply to the Work or any use thereof, including without
     limitation any person's Copyright and Related Rights in the Work.
     Further, Affirmer disclaims responsibility for obtaining any necessary
     consents, permissions or other rights required for any use of the
     Work.
  d. Affirmer understands and acknowledges that Creative Commons is not a
     party to this document and has no duty or obligation with respect to
     this CC0 or use of the Work.
